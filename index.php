<?php

require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("Shaun");
$Frog = new Frog("Buduk");
$Ape = new Ape("kera sakti");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>
<body>
    

    <h1> Relase 0 </h1>

    <?= "Name : ". $sheep->get_name(); ?>
    <br>
    <?= "Legs : ". $sheep->get_legs(); ?>
    <br>
    <?= "cold blooded : ". $sheep->get_cold_blooded(); ?>
    <br>

    <br>
    <br>
    <h1> Relase 1 </h1>

    <?= "Name : ". $Frog->get_name(); ?>
    <br>
    <?= "Legs : ". $Frog->get_legs(); ?>
    <br>
    <?= "cold blooded : ". $Frog->get_cold_blooded(); ?>
    <br>
    <?= "Jump : " . $Frog->jump(); ?>

    <br>
    <br>

    <?= "Name : ". $Ape->get_name(); ?>
    <br>
    <?= "Legs : ". $Ape->get_legs(); ?>
    <br>
    <?= "cold blooded : ". $Ape->get_cold_blooded(); ?>
    <br>
    <?= "Yell : " . $Ape->yell(); ?>
    <br>



</body>
</html>